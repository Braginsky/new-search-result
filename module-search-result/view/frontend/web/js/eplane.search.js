define(
    ['jquery', 'ko', 'uiComponent', 'underscore'],
    function($, ko, Component, _)
    {
        function loadSearchJSON() {
            var json = [];
            $.ajax({
                dataType: "json",
                async: false,
                url: 'http://local.eplane.com/static/frontend/Eplane/main/en_US/Eplane_Search/json/request.json',
                success: function (data) {
                    json = data;
                }
            });

            return json;
        }

        function loadUdpatedJSON() {
            var json = [];
            $.ajax({
                dataType: "json",
                async: false,
                url: 'http://local.eplane.com/static/frontend/Eplane/main/en_US/Eplane_Search/json/requestUpdate1.json',
                success: function (data) {
                    json = data;
                }
            });

            return json;
        }

        function combineByGroupId(items) {
            var data = [];
            for(var itemIndex in items) {
                if(data[items[itemIndex].group_id] == undefined){
                    data[items[itemIndex].group_id] = [];
                }
                data[items[itemIndex].group_id].push(items[itemIndex]);
            }

            var result = [];
            for(var dataIndex in data) {
                result.push(data[dataIndex]);
            }

            return result;
        }
        
        function updateElem(elemToUpdate) {
            if(elemToUpdate.hasClass('hidden')){
                elemToUpdate.removeClass('hidden');
            } else {
                elemToUpdate.addClass('hidden');
            }
        }

        function Filter() {
            this.filters = [];
            this.primaryFilters = [];

            this.loadFilterInitJSON = function() {
                var json = [];
                $.ajax({
                    dataType: "json",
                    async: false,
                    url: 'http://local.eplane.com/static/frontend/Eplane/main/en_US/Eplane_Search/json/filtersInit.json',
                    success: function (data) {
                        json = data;
                    }
                });

                return json;
            };

            this.checkFiltersByFacets = function (initData, facetsData) {
                for(var facetIndex in facetsData) {
                    var facetItems = facetsData[facetIndex].items;
                    for(var itemsIndex in facetItems){
                        var filterIdToCheck = facetItems[itemsIndex].id;
                        if(facetItems[itemsIndex].id !== undefined && facetIndex !== "seller") {
                            for (var dataIndex in initData) {
                                var initDateItem = initData[dataIndex];
                                if (initDateItem.tag == facetIndex) {
                                    var initItems = initDateItem.items;
                                    for (var initIndex in initItems) {
                                        if (initItems[initIndex].id == filterIdToCheck) {
                                            initItems[initIndex].checked = true;
                                            break;
                                        }
                                    }
                                    break;
                                }
                            }
                        }
                    }

                    for(var itemCheckIndex in initData){
                        if(initData[itemCheckIndex].tag == facetIndex){
                            var itemsToCheck = initData[itemCheckIndex].items;
                            for(var checkIndex in itemsToCheck){
                                if(!itemsToCheck[checkIndex].checked){
                                    itemsToCheck[checkIndex].disabled = true;
                                }
                            }
                            break;
                        }
                    }
                }

                return initData;
            };

            this.updateFiltersByFacets = function (facetsData, checkedData) {
                for(var filterIndex in this.filters){
                    for(var facetIndex in facetsData){
                        if(facetIndex == this.filters[filterIndex].tag){
                            var filter = this.filters[filterIndex].items;
                            for (var itemIndex in filter){
                                var facetItems = facetsData[facetIndex].items;
                                var isFilterInFacet = false;
                                for(var itemFacetIndex in facetItems){
                                    if(facetItems[itemFacetIndex].id == filter[itemIndex].id){
                                        if(!filter[itemIndex].checked && filter[itemIndex].disabled){
                                            filter[itemIndex].disabled = false;
                                            filter[itemIndex].checked = true;
                                        }

                                        if(filter[itemIndex].checked && filter[itemIndex].disabled){
                                            filter[itemIndex].disabled = false;
                                        }

                                        isFilterInFacet = true;
                                        break;
                                    }
                                }
                                if(!isFilterInFacet){
                                    if(filter[itemIndex].checked && !filter[itemIndex].disabled &&
                                        (filter[itemIndex].name !== checkedData.name &&
                                       filter[itemIndex].id !== checkedData.id)){
                                        filter[itemIndex].disabled = true;
                                    }
                                }
                            }

                            break;
                        }
                    }


                }
            };

            this.filtersInit = function (facets) {
                var filtersInitData = this.loadFilterInitJSON();
                this.filters = this.checkFiltersByFacets(filtersInitData.filters, facets);

                /* primary filters data */
                this.primaryFilters = this.checkFiltersByFacets(filtersInitData.filters, facets);
            };

            this.filterCheck = function (data, event) {
                var elemToCheck = event.currentTarget.getElementsByTagName('span')[0];

                if (!elemToCheck.classList.contains('checked')) {
                    elemToCheck.classList.add('checked');
                } else {
                    elemToCheck.classList.remove('checked');
                }

               // return elemToCheck.getAttribute('id');
            };

            this.filterCheckOnly = function (data, event) {

                event.stopPropagation();

                /*var elemToCheck = event.currentTarget.firstElementChild;

                /!*var valueToCheck = elemToCheck.getAttribute('value');*!/

                if(!elemToCheck.classList.contains('checked')){
                    elemToCheck.classList.add('checked');
                } else {
                    elemToCheck.classList.remove('checked');
                }*/
            };

            this.createRequest = function () {
                //make request by filters

                return loadUdpatedJSON();
            };

            this.filterUpdate = function (checkedData) {
                var updatedData = this.createRequest();
                this.updateFiltersByFacets(updatedData.facets, checkedData);

                return updatedData.items
            };
        }

        function SearchModel() {
            var self = this;
            self.filter = new Filter();

            self.searchResultHeaders = ko.observableArray([{vendor:'Vendor', vendorRating:'Vendor`s Rating', condition:'Condition', tags:'Tags',
            leadTime:'Lead Time', location:'Location', dealType:'Deal Type', unitPrice:'Unit Price'}]);

            var searchData = loadSearchJSON();
            var searchGroupedData = combineByGroupId(searchData.items);
            self.searchResult = ko.observableArray(searchGroupedData);

            self.currentPage = function (event, currentValue){
                if(currentValue == searchData.page + 1){
                    event.parentElement.classList.add('active');
                }
            };

            self.pageAmount = ko.computed(function () {
                var maxResultsOnPage = 100;
                var maxPagesAmount = 10;
                var pages = Math.ceil(searchData.total / maxResultsOnPage);
                pages = pages <= maxPagesAmount ? pages : maxPagesAmount;
                var result = [];
                for(var pageIndex = 0; pageIndex < pages; pageIndex++){
                    result[pageIndex] = pageIndex + 1;
                }
                return result;
            }, self);

            self.filter.filtersInit(searchData.facets);
            self.filters = ko.observableArray(self.filter.filters);

            self.totalResults = searchData.total + ' results for';

            self.filterElemState = function (data, event) {
                var parentElem = event.currentTarget.parentElement;
                if (parentElem !== undefined && parentElem.classList.contains('expanded')) {
                    parentElem.classList.remove('expanded');
                    parentElem.classList.add('collapsed');
                } else if (parentElem !== undefined && parentElem.classList.contains('collapsed')) {
                    parentElem.classList.remove('collapsed');
                    parentElem.classList.add('expanded');
                }
            };

            self.filterCheck = function(data, event){
                if(!event.currentTarget.classList.contains('disabled')) {
                    self.filter.filterCheck(data, event);
                    data.checked = data.checked ? false : true;
                    var newSearchData = combineByGroupId(self.filter.filterUpdate(data));
                    self.searchResult.removeAll();
                    for(var index in newSearchData) {
                        self.searchResult.push(newSearchData[index]);
                    }

                    var newFilters = JSON.parse(JSON.stringify(self.filter.filters));
                    self.filters.removeAll();
                    for(var filterIndex in newFilters){
                        self.filters.push(newFilters[filterIndex]);
                    }
                }
            };

            self.filterCheckOnly = self.filter.filterCheckOnly;
            
            self.filterToDisable = function (isDisabled, elem, data) {
                if(isDisabled){
                    elem.lastElementChild.lastElementChild.firstElementChild.classList.add('disabled');
                    elem.classList.add('disabled');
                }
            };
            
            self.toSort = function () {
                updateElem($('.sort-options'));
            };

            self.toDynamic = function () {
                updateElem($('.dd-menu'));
            }
        }

        return Component.extend(
        {
            defaults: {
                template: this.getSearchTemplate,
                filterTemplate: this.getFilterTemplate
            },
            
            getSearchTemplate: function () {
                return 'Eplane_Search/search.result';
            },

            getFilterTemplate: function () {
                return 'Eplane_Search/search.filter';
            },

            initialize: function () {
                this._super(new SearchModel());
            }
        });
    }
);