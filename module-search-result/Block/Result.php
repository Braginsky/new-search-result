<?php
/**
 * Copyright © 2016 ePlane. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Eplane\Search\Block;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

/**
 * Product search result block
 */
class Result extends Template
{
    /**
     * @param Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        array $data = []
    )
    {
        parent::__construct($context, $data);
    }
}
